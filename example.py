#!/usr/bin/python
# Copyright 2011  Lars Wirzenius
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import bgproc


import sys
N = int(sys.argv[1])


def func(request):
    return request


bg = bgproc.BackgroundProcessing(func)
numbers = range(N)
results = []
for i in numbers:
    bg.enqueue_request(i)
    results.extend(bg.get_results())
bg.close_requests()
results.extend(bg.get_results(block_all=True))
if numbers != sorted(results):
    print 'N:', N
    print 'len(numbers):', len(numbers)
    print 'len(results):', len(results)
    raise Exception('did not get all results')
bg.finish()


# Copyright 2011  Lars Wirzenius
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest

import bgproc


def callback(request):
    return request + 1


class BackgroundProcessingTests(unittest.TestCase):

    def setUp(self):
        self.bg = bgproc.BackgroundProcessing(callback)
        
    def tearDown(self):
        self.bg.close_requests()
        self.bg.finish()
    
    def test_has_no_pending_initially(self):
        self.assertEqual(self.bg.pending, 0)
    
    def test_get_results_returns_nothing_initially(self):
        self.assertEqual(self.bg.get_results(), [])

    def test_processes_stuff(self):
        self.bg.enqueue_request(0)

        # The following is not a race condition, because the counter
        # only gets decremented after get_results has retrieved the result,
        # and we only call that later.
        self.assertEqual(self.bg.pending, 1)

        self.assertEqual(self.bg.get_results(block=True), [1])
        self.assertEqual(self.bg.pending, 0)
        self.assertEqual(self.bg.get_results(block=True), [])

    def test_gets_all_results(self):
        self.bg.enqueue_request(0)
        self.bg.enqueue_request(1)
        self.assertEqual(sorted(self.bg.get_results(block_all=True)), [1, 2])
        self.assertEqual(self.bg.pending, 0)
        self.assertEqual(self.bg.get_results(block_all=True), [])


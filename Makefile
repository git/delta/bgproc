PYTHON = python

all:

check: all
	$(PYTHON) -m CoverageTestRunner --ignore-missing-from=without-tests
	rm .coverage
	
clean:
	rm -rf *.py[co] build
